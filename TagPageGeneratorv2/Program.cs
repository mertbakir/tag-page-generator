﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagPageGeneratorv2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello human!");
            //This code is created to generate tag pages by looping through all posts for a jekyll blog.
            Console.WriteLine("Enter the exact location of your posts. e.g. blog/_posts or blog/my_collection");

            string location = Console.ReadLine();
            if (location.First() == '"')
            {
                location = location.Substring(1, location.Length - 1);
            }
            if (location.Last() == '"')
            { 
                location = location.Substring(0, location.Length - 1);
            }
            string[] files = Directory.GetFiles(location, "*.md", SearchOption.AllDirectories);
            List<string> tags = new List<string>();
            foreach (var file in files)
            {
                StreamReader reader = new StreamReader($@"{file}");
                string line = "";
                if (reader.ReadLine() != "---")
                {
                    continue; //continue if there is no front-matter;
                }
                //Read until facing the next "---". We don't need to read through all the post.
                while ((line = reader.ReadLine()) != "---")
                {
                    if (line.Contains("tags:"))
                    {
                        string[] tags_temp = line.Split(' ');
                        for (int i = 1; i < tags_temp.Count(); i++)
                        {
                            if (!tags.Contains(tags_temp[i]))
                                tags.Add(tags_temp[i]);
                        }
                        break;
                    }
                }
                reader.Close();
            }


            //Go to blog\tag directory. Find the maindir
            int index = location.LastIndexOf('\\');
            string maindir = location.Substring(0, index);
            string tagsdir = maindir + "\\tag";
            if (Directory.Exists(tagsdir))
            {
                string[] tag_files = Directory.GetFiles(tagsdir, "*.md", SearchOption.AllDirectories);
                foreach (var file in tag_files)
                {
                    File.Delete(file);
                }
            }
            else
            {
                Directory.CreateDirectory(tagsdir);
            }

            foreach (string tag in tags)
            {
                Directory.SetCurrentDirectory($"{tagsdir}");
                string frontmatter = $"---\ntitle: \"#{tag}\"\ntag: {tag}\n---";
                FileStream filestream = new FileStream($"{tag}.md", FileMode.OpenOrCreate, FileAccess.Write);
                var streamwriter = new StreamWriter(filestream);
                streamwriter.WriteLine(frontmatter);
                streamwriter.Close();
                filestream.Close();
            }

            Console.WriteLine("\nTags you ordered are created.\nHave fun, human!");
            Console.ReadKey();
        }
    }
}
