## What is this

* Generates tag pages by looping through all _posts or my_collections (including sub-directories) for a jekyll blog.
* *Will work for windows*
* Reads only front-matter, skips the file if there is no front-matter
* Creates a tag_name.md foreach tag in front-matter of each post.

## Example front-matter of a tag_name.md

```
---
title: "Tag: tag_name"
tag: tag_name
---
```

## You Should

use the code below to set defaults in your ```_config.yml```

```
defaults:
  - scope:
      path: "/tag"
    values:
      layout: "tag"
      robots: noindex, nofollow
```

## How to use

* Download Release.7z
* Run the exe, paste the location of posts directory
* It will remove the old tags under "..\tag\" directory. Not re-write simply remove *all of them.*
* It will generate a tag_name.md in "..\tag\" for each tag.